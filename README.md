## How to use
1. Run App.java as Java Application
2. Hit http://localhost:8080/test/admin and response is "OK - Admin"
3. Hit http://localhost:8080/test/customer and response is "Access is denied"
4. Why /admin and /customer is different response? because in ApiFilter class we defined the Authority to "ADMIN", try change authority to "CUSTOMER" then the response /customer is "OK - Customer" and /admin is "Access is denied" and try to change hasAuthority in TestController also then looks the different.