package com.example.authority;

import java.util.HashMap;
import java.util.Map;

import org.springframework.core.NestedExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorHandler {

	@ExceptionHandler(AccessDeniedException.class)
	public ResponseEntity<?> handlerAccessDeniedException(AccessDeniedException ex) {
		Map<String, Object> map = new HashMap<>();
		map.put("message", NestedExceptionUtils.getMostSpecificCause(ex).getMessage());

		return new ResponseEntity<>(map, HttpStatus.UNAUTHORIZED);

	}
}
