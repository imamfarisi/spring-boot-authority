package com.example.authority;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test")
public class TestController {

	@PreAuthorize("hasAuthority('ADMIN')")
	@GetMapping("admin")
	public String getDataOnLyAdmin() {
		return "OK - Admin";
	}
	
	@PreAuthorize("hasAuthority('CUSTOMER')")
	@GetMapping("customer")
	public String getDataOnlyCustomer() {
		return "OK - Customer";
	}
	
}
